import * as config from "config";

interface IConfig {
  appservice: {
    domainName: string;
    homeserverUrl: string;
    asToken: string;
    hsToken: string;
    id: string,
    url: string,
    userRegex: string,
  };
  webserver: {
    port: number;
    bind: string;
    publicUrl: string;
  };
  media: {
    useLocalCopy: boolean;
    useMediaInfo: boolean;
  };
  dataPath: string;
}

export default <IConfig>config;
